using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MyTestScripttt : MonoBehaviour
{
    public GameObject someGameObject;
    public float someFloat = 15;

    // Start is called before the first frame update
    void Start()
    {
        //gameObject.;
        //Component.Destroy(gameObject, 2);
        //Debug.Log("testttt");
        gameObject.transform.position = Vector3.up * 1f;

        //Debug.Log(gameObject.GetComponent("White"));

        GetComponent<Renderer>().material.SetColor("_Color", Color.red);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    // stabilne vzdy pri vypoctu fyziky
    private void FixedUpdate()
    {
        gameObject.transform.position += Vector3.up * 0.1f;
        //gameObject.GetComponent<Material>().color *= 0.9f;
        
    }

    private void OnTriggerEnter(Collider other)
    {
        Debug.Log("MyTestScripttt" + other.name);
        if (other.name == "unit")
        {

        }
    }
}
