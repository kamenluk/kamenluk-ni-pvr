using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SendUnitColliderScript : MonoBehaviour
{
    private int lifeCounter = 10;
    private List<GameObject> neighbourVertexes = null;
    private VertexScript vertexScript = null;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (lifeCounter <= 0)
            Destroy(gameObject);
        else
            lifeCounter--;
    }

    private void OnTriggerEnter(Collider other)
    {
        //Debug.Log("SendUnitColliderScript trig \"" + other.name + "\"");
        //Debug.Log("SendUnitColliderScript tag: \"" + other.tag + "\"");
        if (other.tag == "Vertex")
        {
            CollisionWithVertex(other);
            Destroy(gameObject);
        }
        
    }

    private void CollisionWithVertex(Collider other)
    {
        if (neighbourVertexes == null)
            throw new Exception();
        foreach (GameObject neighbourVertex in neighbourVertexes)
        {
            if (neighbourVertex.GetInstanceID() == other.gameObject.GetInstanceID())
            {
                vertexScript.SendAllUnitsTowards(other.gameObject);
                return;
            }
        }
        Debug.Log("SendUnitColliderScript nowhere to send units: \"" + "" + "\"");
    }

    internal void SetNeighbours(VertexScript vertexScript)
    {
        this.vertexScript = vertexScript;
        neighbourVertexes = vertexScript.neighbourVertexes;
    }
}
