#if UNITY_EDITOR
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEditor;
using UnityEngine;

[ExecuteInEditMode]
public class VertexScriptEditor : MonoBehaviour
{
    private VertexScript vertexScript = null;
    private TextMeshPro text = null;
    private RectTransform textRectTransform = null;
    private GameObject playerCamera = null;

    // Start is called before the first frame update
    void Start()
    {
        vertexScript = GetComponent<VertexScript>();
    }

    // Update is called once per frame
    void Update()
    {
        if (vertexScript == null)
            vertexScript = GetComponent<VertexScript>();
        vertexScript.UpdateColors();


        text = GetComponentInChildren<TextMeshPro>();
        textRectTransform = GetComponentInChildren<RectTransform>();
        playerCamera = SceneView.lastActiveSceneView.camera.gameObject;

        text.text = vertexScript.unitCount.ToString();
        if (playerCamera != null)
        {
            Vector3 targetTextPos = (playerCamera.transform.position - transform.position).normalized * VertexScript.GetTextDistanceMultip();
            textRectTransform.localPosition = targetTextPos;
            textRectTransform.LookAt(playerCamera.transform.position + (transform.position - playerCamera.transform.position) * 2);
        }
        else
            Debug.Log("playerCamera is null \"" + SceneView.currentDrawingSceneView.camera + "\"");
    }
}
#endif
