#if UNITY_EDITOR
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class EdgeScriptEditor : MonoBehaviour
{
    /* public GameObject vertex1 = null;
     public GameObject vertex2 = null;

     private VertexScript vertexScript1;
     private VertexScript vertexScript2;

     private float scale = 0.1f;*/

    private EdgeScript edgeScript = null;

    // Start is called before the first frame update
    void Start()
    {
        edgeScript = GetComponent<EdgeScript>();
    }

    // Update is called once per frame
    void Update()
    {
        /*if (vertex1 != null && vertex2 != null)
        {
            scale = transform.localScale.x;
            EdgeScript.SetEdgePosition(gameObject, transform, vertex1, vertex2, scale);
        }*/
        if (edgeScript == null)
            edgeScript = GetComponent<EdgeScript>();
        edgeScript.SetEdgePosition();
    }
}
#endif