using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using Valve.VR;
using Valve.VR.InteractionSystem;

public class VertexScript : MonoBehaviour
{
    public int unitCount = 0;
    public int owner = 0;
    public bool generatesUnits = false;
    //[HideInInspector]
    private List<GameObject> edges = new List<GameObject>();
    private GameObject gameController;
    private GameControllerScript gameControllerScript;
    private GameObject unitPrefab;
    private new Renderer renderer;
    //private Material mainMaterial;
    private List<Material> ringsMaterials = new List<Material>();
    private GameObject playerCamera = null;
    private float newUnitSpawnCountdown = 4;
    private float newUnitSpawnCountdownMax = 4;
    //private const int aiSendUnitTresholdBase = 3;
    //private int aiSendUnitTresholdMinInclusive = 3;
    //private int aiSendUnitTresholdMaxInclusive = 6;
    private int aiSendUnitTreshold = 3;

    private static float textDistanceMultip = 0.6f;

    public List<EdgeScript> edgesScripts { get; } = new List<EdgeScript>();
    public List<GameObject> neighbourVertexes { get; } = new List<GameObject>();
    private TextMeshPro text = null;
    private RectTransform textRectTransform = null;

    private void Awake()
    {
        gameController = GameObject.FindGameObjectWithTag("GameController");
        gameControllerScript = gameController.GetComponent<GameControllerScript>();
        unitPrefab = gameControllerScript.unitPrefab;
        //aiSendUnitTresholdMinInclusive = gameControllerScript.aiSendUnitTresholdMinInclusive;
        //aiSendUnitTresholdMaxInclusive = gameControllerScript.aiSendUnitTresholdMaxInclusive;

        if (renderer == null)
            InitializeRendererAndMaterials();
        text = GetComponentInChildren<TextMeshPro>();
        textRectTransform = GetComponentInChildren<RectTransform>();

    }

    // Start is called before the first frame update
    void Start()
    {
        foreach (GameObject edge in edges)
        {
            edgesScripts.Add(edge.GetComponent<EdgeScript>());
            neighbourVertexes.Add(edge.GetComponent<EdgeScript>().OtherVertex(gameObject).gameObject);
        }

        playerCamera = GameObject.Find("VRCamera");
        if (playerCamera == null)
        {
            playerCamera = GameObject.Find("FallbackObjects");
            Debug.Log("UnitScript fallback used \"" + playerCamera + "\"");
        }

        //newUnitSpawnCountdown = UnityEngine.Random.Range(newUnitSpawnCountdownMax - 10, newUnitSpawnCountdownMax);
    }

    // Update is called once per frame
    void Update()
    {
        UpdateColors();

        UpdateText();

        /*if (SteamVR_Input.GetStateUp("GrabGrip", SteamVR_Input_Sources.Any))
        {   // nejaka akce
            Debug.Log("UnitScript qqq \"" + "" + "\"");
        }*/
        
        

    }

    private void UpdateText()
    {
        text.text = unitCount.ToString();

        Vector3 targetTextPos = (playerCamera.transform.position - transform.position).normalized * textDistanceMultip;
        textRectTransform.localPosition = targetTextPos;
        textRectTransform.LookAt(playerCamera.transform.position + (transform.position - playerCamera.transform.position) * 2);
    }

    private void FixedUpdate()
    {
        if (owner != 0)
        {
            if (generatesUnits)
                if (newUnitSpawnCountdown <= 0)
                {
                    unitCount++;
                    newUnitSpawnCountdown = newUnitSpawnCountdownMax;
                }
                else
                    newUnitSpawnCountdown -= Time.deltaTime;


            if (owner != 1)
            {
                if (unitCount >= aiSendUnitTreshold)
                {
                    //CreateNewUnit(0, unitCount);
                    CreateNewUnitOnTargetEdgeIndex(EdgeClosestTowardsEnemy(), unitCount);
                    aiSendUnitTreshold = gameControllerScript.AiSendUnitTreshold(owner);
                }
            }
        }
    }

    public void AddEdge(GameObject edge)
    {
        edges.Add(edge);
    }

    private void CreateNewUnitOnTargetEdgeIndex(int targetEdge, int unitCount)
    {
        if (targetEdge == -1)
            return;

        Vector3 dir = (edgesScripts[targetEdge].OtherVertex(gameObject).transform.position - transform.position).normalized;
        Vector3 newunitPos = transform.position + dir * (transform.lossyScale.x / 2 + unitPrefab.transform.lossyScale.x / 2);
        //GameObject newUnit = Instantiate(unitPrefab, newunitPos, Quaternion.identity);
        GameObject newUnit = Instantiate(unitPrefab, gameController.transform);
        newUnit.transform.position = newunitPos;
        /*newUnit.GetComponent<UnitScript>().SetDirection(dir);
        newUnit.GetComponent<UnitScript>().SetSourceVertex(gameObject);*/
        UnitScript unitScript = newUnit.GetComponent<UnitScript>();
        unitScript.direction = dir;
        unitScript.sourceVertex = gameObject;
        unitScript.owner = owner;
        unitScript.unitCount = unitCount;

        this.unitCount -= unitCount;
    }

    private bool CreateNewUnitOnTargetGameObject(GameObject target, int unitCount)
    {
        if (target.tag == "Vertex")
        {
            for (int i = 0; i < edgesScripts.Count; i++)
            {
                if (edgesScripts[i].OtherVertex(gameObject).GetInstanceID() == target.GetInstanceID())
                {
                    CreateNewUnitOnTargetEdgeIndex(i, unitCount);
                    return true;
                }
            }
        }
        else if (target.tag == "Edge")
        {
            for (int i = 0; i < edgesScripts.Count; i++)
            {
                if (edges[i].GetInstanceID() == target.GetInstanceID())
                {
                    CreateNewUnitOnTargetEdgeIndex(i, unitCount);
                    return true;
                }
            }
        }
        return false;
    }

    public void SendAllUnitsTowards(GameObject target)
    {
        CreateNewUnitOnTargetGameObject(target, unitCount);
    }

    private int EdgeClosestTowardsEnemy()
    {
        SortedList<float, VertexScript> q = new SortedList<float, VertexScript>(new DuplicateKeyComparer<float>());
        List<int> closed = new List<int>(); //ID
        Dictionary<int, int> prev = new Dictionary<int, int>(); //ID to ID
        Dictionary<int, float> d = new Dictionary<int, float>(); //ID to dist

        q.Add(0, this);
        prev.Add(gameObject.GetInstanceID(), 0);
        d.Add(gameObject.GetInstanceID(), 0);

        while (q.Count > 0)
        {
            VertexScript u = q.Values[0];
            q.RemoveAt(0);
            int uID = u.gameObject.GetInstanceID();

            if (u.owner != owner)
            {
                int vertexID = uID;
                int targetID = -1;
                while (vertexID != gameObject.GetInstanceID())
                {
                    targetID = vertexID;
                    vertexID = prev[vertexID];
                }
                for (int i = 0; i < edgesScripts.Count; i++)
                {
                    if (edgesScripts[i].OtherVertex(gameObject).GetInstanceID() == targetID)
                        return i;
                }
                throw new Exception();
            }

            for (int i = 0; i < u.edgesScripts.Count; i++)
            {
                GameObject otherVertex = u.edgesScripts[i].OtherVertex(u.gameObject);
                float dist = d[uID] + (u.gameObject.transform.position - otherVertex.transform.position).magnitude;
                bool otherVertexDistExist = d.TryGetValue(otherVertex.GetInstanceID(), out float otherVertexDist);
                if (!closed.Contains(uID) && (otherVertexDistExist == false || dist < otherVertexDist))
                {
                    if (prev.ContainsKey(otherVertex.GetInstanceID()))
                    {
                        prev.Remove(otherVertex.GetInstanceID());
                    }
                    prev.Add(otherVertex.GetInstanceID(), uID);
                    if(d.ContainsKey(otherVertex.GetInstanceID()))
                    {
                        d.Remove(otherVertex.GetInstanceID());
                    }
                    d.Add(otherVertex.GetInstanceID(), dist);

                    if (q.IndexOfValue(u) != -1)
                        q.RemoveAt(q.IndexOfValue(u));
                    q.Add(dist, otherVertex.GetComponent<VertexScript>());
                }
            }
            closed.Add(uID);
        }
        return -1;
    }

    public void UpdateColors()
    {
        Color color = GameControllerScript.OwnerColor(owner);

        if (renderer == null)
            InitializeRendererAndMaterials();

        foreach (Material material in ringsMaterials)
        {
            material.SetColor("_Color", color * 0.7f);
        }
        renderer.sharedMaterial.SetColor("_Color", color);

        if (owner != 0)
        {
            renderer.sharedMaterial.EnableKeyword("_EMISSION");
            renderer.sharedMaterial.SetColor("_EmissionColor", color * 1.8f);
        }
        else
        {
            renderer.sharedMaterial.DisableKeyword("_EMISSION");
        }
    }

    private void InitializeRendererAndMaterials()
    {
        renderer = GetComponent<Renderer>();
        var tempMaterial = new Material(renderer.sharedMaterial);
        renderer.sharedMaterial = tempMaterial;
        //mainMaterial = tempMaterial;

        ringsMaterials = new List<Material>();
        foreach (Renderer rend in GetComponentsInChildren<Renderer>())
        {
            tempMaterial = new Material(rend.sharedMaterial);
            rend.sharedMaterial = tempMaterial;
            ringsMaterials.Add(tempMaterial);
        }
    }

    public static float GetTextDistanceMultip()
    {
        return textDistanceMultip;
    }






    /// <summary>
    /// Comparer for comparing two keys, handling equality as beeing greater
    /// Use this Comparer e.g. with SortedLists or SortedDictionaries, that don't allow duplicate keys
    /// </summary>
    /// <typeparam name="TKey"></typeparam>
    public class DuplicateKeyComparer<TKey>
                    :
                 IComparer<TKey> where TKey : IComparable
    {
        #region IComparer<TKey> Members

        public int Compare(TKey x, TKey y)
        {
            int result = x.CompareTo(y);

            if (result == 0)
                return 1;   // Handle equality as beeing greater
            else
                return result;
        }

        #endregion
    }
}
