using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class UnitScript : MonoBehaviour
{
    public int unitCount = 0;
    public int owner = 0;
    public Vector3 direction;
    private float speed = 0.0006f;
    public GameObject sourceVertex;
    private GameObject playerCamera = null;

    private static float textDistanceMultip = 0.5f;

    private void Awake()
    {
        //playerPos = GameObject.FindGameObjectWithTag("Player").GetComponent<Transform>();
        //Debug.Log("UnitScript test \"" + playerPos + "\"");

        
    }

    // Start is called before the first frame update
    void Start()
    {
        /*switch (owner)
        {
            case 0:
                GetComponent<Renderer>().material.SetColor("_Color", Color.white);
                break;
            case 1:
                GetComponent<Renderer>().material.SetColor("_Color", Color.blue);
                break;
            case 2:
                GetComponent<Renderer>().material.SetColor("_Color", Color.red);
                break;
            default:
                //throw new System.Exception();
                break;
        }*/
        GetComponent<Renderer>().material.SetColor("_Color", GameControllerScript.OwnerColor(owner));

        playerCamera = GameObject.Find("VRCamera");
        if (playerCamera == null)
        {
            playerCamera = GameObject.Find("FallbackObjects");
            //Debug.Log("UnitScript fallback used \"" + playerCamera + "\"");
        }

        //Debug.Log("UnitScript test \"" + GetComponentInChildren<TextMeshPro>() + "\"");
        //GetComponentInChildren<TextMeshPro>().text = unitCount.ToString();

    }

    // Update is called once per frame
    void Update()
    {
        //Debug.Log("UnitScript test \"" + playerCamera.transform.localPosition + "\"");
        Vector3 targetTextPos = (playerCamera.transform.position - transform.position).normalized * textDistanceMultip;
        GetComponentInChildren<RectTransform>().localPosition = targetTextPos;
        GetComponentInChildren<RectTransform>().LookAt(playerCamera.transform.position + (transform.position - playerCamera.transform.position) * 2);
        GetComponentInChildren<TextMeshPro>().text = unitCount.ToString();
    }

    private void FixedUpdate()
    {
        gameObject.transform.position += direction * speed;
    }

    private void OnTriggerEnter(Collider other)
    {
        //Debug.Log("UnitScript trig \"" + other.name + "\"");
        //Debug.Log("UnitScript tag: \"" + other.tag + "\"");
        if (unitCount == 0)
        {
            Destroy(gameObject);
            return;
        }
        if (other.tag == "Unit")
        {
            CollisionWithUnit(other);
        }
        else if (other.tag == "Vertex")
        {
            CollisionWithVertex(other);
        }
        else
        {
            //Debug.Log("UnitScript collision with \"" + other.name + "\"");
        }
    }

    private void CollisionWithVertex(Collider other)
    {
        VertexScript vertexScript = other.GetComponent<VertexScript>();
        //if (other.gameObject == sourceVertex && vertexScript.owner == owner)
        if (other.gameObject == sourceVertex)
            return;
        if (vertexScript.owner == owner)
            vertexScript.unitCount += unitCount;
        else if (vertexScript.owner != owner)
        {
            if (vertexScript.unitCount >= unitCount)
                vertexScript.unitCount -= unitCount;
            else if (vertexScript.unitCount < unitCount)
            {
                vertexScript.unitCount = unitCount - vertexScript.unitCount;
                vertexScript.owner = owner;
            }
        }
        unitCount = 0;
        Destroy(gameObject);
    }

    private void CollisionWithUnit(Collider other)
    {
        UnitScript otherUnitScript = other.GetComponent<UnitScript>();
        if (otherUnitScript.owner == owner)
            MergeUnits(otherUnitScript);

        int otherUnitCountTmp = otherUnitScript.unitCount;
        otherUnitScript.unitCount -= unitCount;
        if (otherUnitScript.unitCount < 0)
            otherUnitScript.unitCount = 0;
        unitCount -= otherUnitCountTmp;
        if (unitCount < 0)
            unitCount = 0;

        if (unitCount == 0)
        {
            Destroy(gameObject);
        }
        if (otherUnitScript.unitCount == 0)
        {
            Destroy(other.gameObject);
        }
    }

    private void MergeUnits(UnitScript other)
    {
        unitCount += other.unitCount;
        other.unitCount = 0;
        transform.position = (transform.position + other.transform.position) / 2;
        Destroy(other.gameObject);
    }

    /*public void SetDirection(Vector3 direction)
    {
        this.direction = direction;
    }

    public void SetSourceVertex(GameObject sourceVertex)
    {
        this.sourceVertex = sourceVertex;
    }*/
}
