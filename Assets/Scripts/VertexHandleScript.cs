using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Valve.VR.InteractionSystem;
using Valve.VR;
using System;
using System.IO;

public class VertexHandleScript : MonoBehaviour
{
    private Interactable interactable;
    private Hand.AttachmentFlags attachmentFlags = Hand.defaultAttachmentFlags & (~Hand.AttachmentFlags.SnapOnAttach) & (~Hand.AttachmentFlags.DetachOthers) & (~Hand.AttachmentFlags.VelocityMovement);
    private GameObject sendUnitColliderPrefab;
    //private Hand.AttachmentFlags attachmentFlags = Hand.defaultAttachmentFlags;
    //private Vector3 oldPosition;
    private VertexScript vertexScript;


    private void Awake()
    {
        interactable = this.GetComponent<Interactable>();
        vertexScript = GetComponentInParent<VertexScript>();
        sendUnitColliderPrefab = (GameObject)GameControllerScript.LoadPrefabFromFile("Prefabs/SendUnitCollider");
        

    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnHandHoverBegin(Hand hand)
    {
        
    }

    private void HandHoverUpdate(Hand hand)
    {
        GrabTypes startingGrabType = hand.GetGrabStarting();
        bool isGrabEnding = hand.IsGrabEnding(this.gameObject);

        if (interactable.attachedToHand == null && startingGrabType != GrabTypes.None && vertexScript.owner == 1)
        {
            // Save our position/rotation so that we can restore it when we detach
            //oldPosition = transform.position;
            //oldRotation = transform.rotation;

            // Call this to continue receiving HandHoverUpdate messages,
            // and prevent the hand from hovering over anything else
            hand.HoverLock(interactable);

            // Attach this object to the hand
            hand.AttachObject(gameObject, startingGrabType, attachmentFlags);
            //Debug.Log("UnitScript Grab Begin \"" + "" + "\"");
        }
        else if (isGrabEnding)
        {
            // Detach this object from the hand
            hand.DetachObject(gameObject);

            // Call this to undo HoverLock
            hand.HoverUnlock(interactable);

            if (vertexScript.owner == 1)
                CreateTrigger();

            // Restore position/rotation
            //transform.position = oldPosition;
            transform.localPosition = new Vector3(0,0,0);
            //transform.rotation = oldRotation;
            //Debug.Log("UnitScript Grab End \"" + "" + "\"");
        }
    }

    private void CreateTrigger()
    {
        GameObject collider = Instantiate(sendUnitColliderPrefab, transform.position, Quaternion.identity);
        collider.transform.localScale = transform.lossyScale;
        SendUnitColliderScript sendUnitColliderScript = collider.GetComponent<SendUnitColliderScript>();
        sendUnitColliderScript.SetNeighbours(vertexScript);
        //Debug.Log("UnitScript CreateTrigger \"" + collider + "\"");
    }

    /*private void OnAttachedToHand(Hand hand)
    {
        Debug.Log("UnitScript OAH \"" + "" + "\"");
    }

    private void HandAttachedUpdate(Hand hand)
    {
        Debug.Log("UnitScript HAU \"" + "" + "\"");
    }*/

}
