using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EdgeScript : MonoBehaviour
{
    public GameObject vertex1;
    public GameObject vertex2;

    private VertexScript vertexScript1;
    private VertexScript vertexScript2;

    private float scale = 0.1f;

    private void Awake()
    {
        scale = transform.localScale.x;
        
        SetEdgePosition();

        vertexScript1 = vertex1.GetComponent<VertexScript>();
        vertexScript2 = vertex2.GetComponent<VertexScript>();
        vertexScript1.AddEdge(gameObject);
        vertexScript2.AddEdge(gameObject);
    }

    // Start is called before the first frame update
    void Start()
    {
        //SetEdgePosition();
    }

    // Update is called once per frame
    void Update()
    {
        //SetEdgePosition();
    }

    public GameObject OtherVertex(GameObject vertex)
    {
        if (vertex == vertex1)
            return vertex2;
        else if (vertex == vertex2)
            return vertex1;
        Debug.Log("OtherVertex not found vertex in parameter.");
        return null;
    }

    /*private void SetEdgePosition()
    {
        SetEdgePosition(gameObject, transform, vertex1, vertex2, scale);
    }

    public static void SetEdgePosition(GameObject gameObject, Transform transform, GameObject vertex1, GameObject vertex2, float scale)
    {
        gameObject.transform.position = vertex1.transform.position + (vertex2.transform.position - vertex1.transform.position) / 2;
        //gameObject.transform.rotation = Quaternion.LookRotation((vertex2.transform.position - vertex1.transform.position).normalized);
        transform.LookAt(vertex1.transform.position);
        transform.localScale = new Vector3(scale, scale, 1 / transform.lossyScale.y * transform.localScale.y * (vertex2.transform.position - vertex1.transform.position).magnitude / 2);
    }*/

    public void SetEdgePosition()
    {
        if (vertex1 == null || vertex2 == null)
            return;

        gameObject.transform.position = vertex1.transform.position + (vertex2.transform.position - vertex1.transform.position) / 2;
        //gameObject.transform.rotation = Quaternion.LookRotation((vertex2.transform.position - vertex1.transform.position).normalized);
        transform.LookAt(vertex1.transform.position);
        transform.localScale = new Vector3(scale, scale, 1 / transform.lossyScale.y * transform.localScale.y * (vertex2.transform.position - vertex1.transform.position).magnitude / 2);
    }
}
