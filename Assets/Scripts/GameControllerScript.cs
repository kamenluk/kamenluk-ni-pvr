using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEditor;
using UnityEngine;
using UnityEngine.SceneManagement;
using Valve.VR;
using Valve.VR.InteractionSystem;
//using Valve.VR.InteractionSystem;

public class GameControllerScript : MonoBehaviour
{
    //public int scoreee = 0;
    public GameObject unitPrefab;

    public SteamVR_Action_Vector2 touchpad;
    public SteamVR_Action_Boolean buttonPressMoveMap;
    private Vector3 handPositionWhenButtonPressed;
    private Vector3 mapPositionWhenPressed;
    private Quaternion handRotationWhenButtonPressed;
    private Quaternion mapRotationWhenPressed;
    public SteamVR_Input_Sources handLeft;
    public SteamVR_Input_Sources handRight;
    public SteamVR_Input_Sources handAny;
    public SteamVR_Action_Boolean buttonPressNextMap;
    public SteamVR_Action_Boolean buttonPressResetMap;
    public SteamVR_Action_Boolean buttonPressPreviousMap;

    public int aiSendUnitTresholdMinInclusive = 3;
    public int aiSendUnitTresholdMaxInclusive = 6;
    public int aiSendUnitTresholdIncPerVertexes = 3;

    public Material skyBox = null;

    private GameObject leftHand;
    private GameObject rightHand;

    public int currentLevel = 0;
    private const int maxLevel = 4;


    private VertexScript[] vertexScripts;
    //private UnitScript[] unitScripts;

    // Start is called before the first frame update
    void Start()
    {
        leftHand = GameObject.Find("LeftHand");
        rightHand = GameObject.Find("RightHand");
        if (leftHand == null || rightHand == null)
        {
            leftHand = GameObject.Find("FallbackHand");
            rightHand = leftHand;
            Debug.Log("GameControllerScript fallback used - no Left/Right Hand - using \"" + leftHand + "\"");
        }
        if (leftHand == null)
            throw new System.Exception();

        if (skyBox != null)
        {
            RenderSettings.skybox = skyBox;
        }

        //PlayerPrefs.SetInt("Map", 0);


        vertexScripts = GetComponentsInChildren<VertexScript>();
        //NextLevel();
    }

    // Update is called once per frame
    void Update()
    {
        /*validPos = UpdatePointer();
        pointer.SetActive(validPos);*/

        /*if (touchpad.GetChanged(hand))
        {
            gameController.transform.position += new Vector3(touchpad.axis.x, 0, touchpad.axis.y) * Time.deltaTime;
            Debug.Log("UnitScript qqq \"" + new Vector3(touchpad.axis.x, 0, touchpad.axis.y) * Time.deltaTime + "\"");
        }*/


        //transform.position += new Vector3(touchpad.axis.x, 0, touchpad.axis.y) * Time.deltaTime;

        if (buttonPressMoveMap.GetStateDown(handLeft))
        {
            handPositionWhenButtonPressed = leftHand.transform.position;
            mapPositionWhenPressed = transform.position;

            handRotationWhenButtonPressed = leftHand.transform.rotation;
            mapRotationWhenPressed = transform.rotation;

            //Debug.Log("UnitScript buttonPressed GetStateDown, handPositionWhenButtonPressed = \"" + handPositionWhenButtonPressed + "\"");
        }
        if (buttonPressMoveMap.GetState(handLeft))
        {
            //transform.position += new Vector3(touchpad.axis.x, 0, touchpad.axis.y) * Time.deltaTime;
            Vector3 change = (leftHand.transform.position - handPositionWhenButtonPressed) / 1f;
            transform.position = mapPositionWhenPressed + change;

            //transform.rotation = mapRotationWhenPressed;
            //Vector3 changeRot = (leftHand.transform.rotation.eulerAngles - handRotationWhenButtonPressed.eulerAngles) / 1f;
            //transform.eulerAngles = mapRotationWhenPressed.eulerAngles + changeRot;
            //transform.RotateAround(transform.position, changeRot.normalized, changeRot.magnitude);
            //transform.Rotate(changeRot, Space.Self)�


            //Debug.Log("UnitScript buttonPressed GetState, leftHand.transform.position = \"" + leftHand.transform.position + "\"");
            //Debug.Log("UnitScript buttonPressed GetState, change = \"" + change + "\"");
        }
        if (buttonPressNextMap != null && buttonPressNextMap.GetStateDown(handLeft))
            NextLevel();
        if (buttonPressNextMap != null && buttonPressResetMap.GetStateDown(handLeft))
            RestartLevel();
        if (buttonPressNextMap != null && buttonPressPreviousMap.GetStateDown(handLeft))
            PreviousLevel();

    }

    private void FixedUpdate()
    {
        NextLevelControl();
        RestatrLevelControl();
        //NextLevel();
    }

    private void RestatrLevelControl()
    {
        int playerOwner = 1;
        foreach (VertexScript vertex in vertexScripts)
        {
            if (playerOwner == vertex.owner)
                return;
        }
        foreach (UnitScript unit in GetComponentsInChildren<UnitScript>())
        {
            if (playerOwner == unit.owner)
                return;
        }
        RestartLevel();
    }

    private void NextLevelControl()
    {
        int someOwner = -1;
        foreach (VertexScript vertex in vertexScripts)
        {
            if (someOwner == -1)
                someOwner = vertex.owner;
            if (someOwner != vertex.owner)
                return;
        }
        foreach (UnitScript unit in GetComponentsInChildren<UnitScript>())
        {
            if (someOwner == -1)
                someOwner = unit.owner;
            if (someOwner != unit.owner)
                return;
        }
        NextLevel();
    }

    public static Color OwnerColor(int owner)
    {
        Color color = Color.white;
        switch (owner)
        {
            case 0:
                color = Color.gray;
                break;
            case 1:
                color = Color.blue;
                break;
            case 2:
                color = Color.red;
                break;
            case 3:
                color = Color.green;
                break;
            case 4:
                color = Color.yellow;
                break;
            case 5:
                color = Color.magenta;
                break;
            default:
                Debug.Log("Trying to load color for owner " + owner + "");
                throw new System.Exception("Trying to load color for owner " + owner + "");
                //break;
        }
        return color;
    }

    /*private Vector3 UpdatePointer()
    {
        Ray ray = new Ray(transform.position, transform.forward);
        RaycastHit hit;

        Physics.Raycast(ray, out hit, 10f, 1 << 6);

        return hit.point;
    }*/



    public static UnityEngine.Object LoadPrefabFromFile(string filename)
    {
        Debug.Log("Trying to load LevelPrefab from file (" + filename + ")...");
        var loadedObject = Resources.Load(filename);
        if (loadedObject == null)
        {
            throw new FileNotFoundException("...no file found - please check the configuration");
        }
        return loadedObject;
    }

    /*IEnumerator NextLevel()
    {
        var currentLevel = PlayerPrefs.GetInt("Map", 0);
        var nextLevel = currentLevel + 1;
        Debug.Log("Will attempt to load map = \"" + nextLevel + "\"");
        if (nextLevel >= maxLevel)
        {
            Debug.LogWarning("No more levels available");
            yield break;
        }

        PlayerPrefs.SetInt("Map", nextLevel);

        Debug.Log("wwww");
        yield return new WaitForSeconds(1);
        Debug.Log("eeeee");
        SceneManager.LoadScene(nextLevel);
        Debug.Log("rrrrrrr");
    }*/

    /*private void NextLevel()
    {
        int currentLevel = PlayerPrefs.GetInt("Map", 0);
        int nextLevel = currentLevel + 1;
        Debug.Log("Will attempt to load map = \"" + nextLevel + "\"");
        if (nextLevel >= maxLevel)
        {
            Debug.LogWarning("No more levels available");
            return;
        }

        PlayerPrefs.SetInt("Map", nextLevel);
        PlayerPrefs.Save();

        SceneManager.LoadScene(nextLevel);
    }*/

    private void NextLevel()
    {
        int nextLevel = currentLevel + 1;
        Debug.Log("Will attempt to load map = \"" + nextLevel + "\"");
        if (nextLevel >= maxLevel)
        {
            Debug.LogWarning("No more levels available -> restarting level");
            SceneManager.LoadScene(1);
            return;
        }

        SceneManager.LoadScene(nextLevel);
    }

    private void RestartLevel()
    {
        SceneManager.LoadScene(currentLevel);
    }

    private void PreviousLevel()
    {
        int nextLevel = currentLevel - 1;
        Debug.Log("Will attempt to load map = \"" + nextLevel + "\"");
        if (nextLevel < 0)
        {
            Debug.LogWarning("Trying to load map -1");
            return;
        }

        SceneManager.LoadScene(nextLevel);
    }

    public int AiSendUnitTreshold(int owner)
    {
        int res = UnityEngine.Random.Range(aiSendUnitTresholdMinInclusive, aiSendUnitTresholdMaxInclusive + 1);

        int vertexCnt = 0;
        foreach (VertexScript vertex in vertexScripts)
        {
            if (owner == vertex.owner)
                vertexCnt++;
        }
        res += vertexCnt / aiSendUnitTresholdIncPerVertexes;

        return res;
    }
}
